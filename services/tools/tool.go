package tool

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func GetRoot(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "Hello Golang",
	})
}

func GetPing(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "pong",
	})
}

func GetWhois(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": c.ClientIP(),
	})
}

func Echo(c *gin.Context) {
	echo := c.Params.ByName("echo")
	c.JSON(http.StatusOK, gin.H{
		"message": echo,
	})
}
