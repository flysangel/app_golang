package router

import (
	data "main/services/datas"
	system "main/services/systems"
	tool "main/services/tools"

	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	r := gin.Default()

	// tools
	r.GET("/", tool.GetRoot)
	r.GET("/ping", tool.GetPing)
	r.GET("/whois", tool.GetWhois)
	r.GET("/:echo", tool.Echo)

	// system
	r.GET("/time", system.GetTime)

	r.GET("/host", system.GetHost)
	r.GET("/hostname", system.GetHostname)
	r.GET("/cpu", system.GetCPU)
	r.GET("/mem", system.GetMEM)
	r.GET("/disk", system.GetDisk)
	r.GET("/net", system.GetNet)

	// datas
	r.GET("/datas", data.GetDatas)
	r.GET("/data/:name", data.GetData)

	return r
}
