package data

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"net/http"

	"github.com/gin-gonic/gin"
)

func GetUrl(c *gin.Context, s string) string {
	response, _ := http.Get(s)
	responseData, _ := ioutil.ReadAll(response.Body)
	return string(responseData)
}

func GetDatas(c *gin.Context) {
	responseData := GetUrl(c, "https://gitlab.com/flysangel/app_go/raw/main/mapping.json")
	c.String(http.StatusOK, "%s", responseData)
}

func GetData(c *gin.Context) {
	dataname := c.Params.ByName("name")
	mappingData := GetUrl(c, "https://gitlab.com/flysangel/app_go/raw/main/mapping.json")

	// Declared an empty interface
	var result map[string]interface{}

	// Unmarshal or Decode the JSON to the interface.
	json.Unmarshal([]byte(mappingData), &result)
	dataurl := fmt.Sprintf("%v", result[dataname])

	responseData := GetUrl(c, dataurl)
	c.String(http.StatusOK, "%s", responseData)
}
