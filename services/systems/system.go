package system

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/disk"
	"github.com/shirou/gopsutil/host"
	"github.com/shirou/gopsutil/mem"
	"github.com/shirou/gopsutil/net"
)

func GetTime(c *gin.Context) {
	dt := time.Now()
	c.JSON(http.StatusOK, gin.H{
		"message": dt.Format("2006/01/02 15:04:05"),
	})
}

func GetHost(c *gin.Context) {
	hostStat, _ := host.Info()
	c.JSON(http.StatusOK, gin.H{
		"message": hostStat,
	})
}

func GetHostname(c *gin.Context) {
	hostStat, _ := host.Info()
	c.JSON(http.StatusOK, gin.H{
		"message": hostStat.Hostname,
	})
}

func GetCPU(c *gin.Context) {
	cpuStat, _ := cpu.Info()
	c.JSON(http.StatusOK, gin.H{
		"message": cpuStat,
	})
}

func GetMEM(c *gin.Context) {
	vmStat, _ := mem.VirtualMemory()
	c.JSON(http.StatusOK, gin.H{
		"message": vmStat,
	})
}

func GetDisk(c *gin.Context) {
	diskStat, _ := disk.Usage("/")
	c.JSON(http.StatusOK, gin.H{
		"message": diskStat,
	})
}

func GetNet(c *gin.Context) {
	interfStat, _ := net.Interfaces()
	c.JSON(http.StatusOK, gin.H{
		"message": interfStat,
	})
}
